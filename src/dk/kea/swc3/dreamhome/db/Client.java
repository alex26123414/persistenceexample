/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findByClientNo", query = "SELECT c FROM Client c WHERE c.clientNo = :clientNo"),
    @NamedQuery(name = "Client.findByFName", query = "SELECT c FROM Client c WHERE c.fName = :fName"),
    @NamedQuery(name = "Client.findByLName", query = "SELECT c FROM Client c WHERE c.lName = :lName"),
    @NamedQuery(name = "Client.findByTelNo", query = "SELECT c FROM Client c WHERE c.telNo = :telNo"),
    @NamedQuery(name = "Client.findByStreet", query = "SELECT c FROM Client c WHERE c.street = :street"),
    @NamedQuery(name = "Client.findByCity", query = "SELECT c FROM Client c WHERE c.city = :city"),
    @NamedQuery(name = "Client.findByPostCode", query = "SELECT c FROM Client c WHERE c.postCode = :postCode"),
    @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email"),
    @NamedQuery(name = "Client.findByJoinedOn", query = "SELECT c FROM Client c WHERE c.joinedOn = :joinedOn"),
    @NamedQuery(name = "Client.findByRegion", query = "SELECT c FROM Client c WHERE c.region = :region"),
    @NamedQuery(name = "Client.findByPreType", query = "SELECT c FROM Client c WHERE c.preType = :preType"),
    @NamedQuery(name = "Client.findByMaxRent", query = "SELECT c FROM Client c WHERE c.maxRent = :maxRent")})
public class Client implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "clientNo")
    private String clientNo;
    @Column(name = "fName")
    private String fName;
    @Column(name = "lName")
    private String lName;
    @Column(name = "telNo")
    private String telNo;
    @Column(name = "street")
    private String street;
    @Column(name = "city")
    private String city;
    @Column(name = "postCode")
    private String postCode;
    @Column(name = "email")
    private String email;
    @Column(name = "joinedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date joinedOn;
    @Column(name = "region")
    private String region;
    @Column(name = "preType")
    private String preType;
    @Column(name = "maxRent")
    private Integer maxRent;
    @OneToMany(mappedBy = "clientID")
    private Collection<Viewing> viewingCollection;

    public Client() {
    }

    public Client(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getFName() {
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getLName() {
        return lName;
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getJoinedOn() {
        return joinedOn;
    }

    public void setJoinedOn(Date joinedOn) {
        this.joinedOn = joinedOn;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPreType() {
        return preType;
    }

    public void setPreType(String preType) {
        this.preType = preType;
    }

    public Integer getMaxRent() {
        return maxRent;
    }

    public void setMaxRent(Integer maxRent) {
        this.maxRent = maxRent;
    }

    @XmlTransient
    public Collection<Viewing> getViewingCollection() {
        return viewingCollection;
    }

    public void setViewingCollection(Collection<Viewing> viewingCollection) {
        this.viewingCollection = viewingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientNo != null ? clientNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.clientNo == null && other.clientNo != null) || (this.clientNo != null && !this.clientNo.equals(other.clientNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Client[ clientNo=" + clientNo + " ]";
    }
    
}
