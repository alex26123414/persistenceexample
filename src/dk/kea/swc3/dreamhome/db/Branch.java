/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "branch")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Branch.findAll", query = "SELECT b FROM Branch b"),
    @NamedQuery(name = "Branch.findByBranchNo", query = "SELECT b FROM Branch b WHERE b.branchNo = :branchNo"),
    @NamedQuery(name = "Branch.findByStreet", query = "SELECT b FROM Branch b WHERE b.street = :street"),
    @NamedQuery(name = "Branch.findByCity", query = "SELECT b FROM Branch b WHERE b.city = :city"),
    @NamedQuery(name = "Branch.findByPostcode", query = "SELECT b FROM Branch b WHERE b.postcode = :postcode")})
public class Branch implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "branchNo")
    private String branchNo;
    @Column(name = "street")
    private String street;
    @Column(name = "city")
    private String city;
    @Column(name = "postcode")
    private String postcode;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "branchNo")
    private Collection<Propertyforrent> propertyforrentCollection;
    @OneToMany(mappedBy = "branchNo")
    private Collection<Staff> staffCollection;

    public Branch() {
    }

    public Branch(String branchNo) {
        this.branchNo = branchNo;
    }

    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @XmlTransient
    public Collection<Propertyforrent> getPropertyforrentCollection() {
        return propertyforrentCollection;
    }

    public void setPropertyforrentCollection(Collection<Propertyforrent> propertyforrentCollection) {
        this.propertyforrentCollection = propertyforrentCollection;
    }

    @XmlTransient
    public Collection<Staff> getStaffCollection() {
        return staffCollection;
    }

    public void setStaffCollection(Collection<Staff> staffCollection) {
        this.staffCollection = staffCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (branchNo != null ? branchNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Branch)) {
            return false;
        }
        Branch other = (Branch) object;
        if ((this.branchNo == null && other.branchNo != null) || (this.branchNo != null && !this.branchNo.equals(other.branchNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Branch[ branchNo=" + branchNo + " ]";
    }
    
}
