/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "viewing")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Viewing.findAll", query = "SELECT v FROM Viewing v"),
    @NamedQuery(name = "Viewing.findById", query = "SELECT v FROM Viewing v WHERE v.id = :id"),
    @NamedQuery(name = "Viewing.findByViewDate", query = "SELECT v FROM Viewing v WHERE v.viewDate = :viewDate"),
    @NamedQuery(name = "Viewing.findByViewHour", query = "SELECT v FROM Viewing v WHERE v.viewHour = :viewHour"),
    @NamedQuery(name = "Viewing.findByComment", query = "SELECT v FROM Viewing v WHERE v.comment = :comment"),
    @NamedQuery(name = "Viewing.findByWishToRent", query = "SELECT v FROM Viewing v WHERE v.wishToRent = :wishToRent")})
public class Viewing implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "viewDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date viewDate;
    @Column(name = "viewHour")
    private String viewHour;
    @Column(name = "comment")
    private String comment;
    @Column(name = "wishToRent")
    private Boolean wishToRent;
    @JoinColumn(name = "clientID", referencedColumnName = "clientNo")
    @ManyToOne
    private Client clientID;
    @JoinColumn(name = "propertyNo", referencedColumnName = "propertyNo")
    @ManyToOne
    private Propertyforrent propertyNo;

    public Viewing() {
    }

    public Viewing(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getViewDate() {
        return viewDate;
    }

    public void setViewDate(Date viewDate) {
        this.viewDate = viewDate;
    }

    public String getViewHour() {
        return viewHour;
    }

    public void setViewHour(String viewHour) {
        this.viewHour = viewHour;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getWishToRent() {
        return wishToRent;
    }

    public void setWishToRent(Boolean wishToRent) {
        this.wishToRent = wishToRent;
    }

    public Client getClientID() {
        return clientID;
    }

    public void setClientID(Client clientID) {
        this.clientID = clientID;
    }

    public Propertyforrent getPropertyNo() {
        return propertyNo;
    }

    public void setPropertyNo(Propertyforrent propertyNo) {
        this.propertyNo = propertyNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viewing)) {
            return false;
        }
        Viewing other = (Viewing) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Viewing[ id=" + id + " ]";
    }
    
}
