/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "coffees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Coffees.findAll", query = "SELECT c FROM Coffees c"),
    @NamedQuery(name = "Coffees.findById", query = "SELECT c FROM Coffees c WHERE c.id = :id"),
    @NamedQuery(name = "Coffees.findBySales", query = "SELECT c FROM Coffees c WHERE c.sales = :sales"),
    @NamedQuery(name = "Coffees.findByCofName", query = "SELECT c FROM Coffees c WHERE c.cofName = :cofName"),
    @NamedQuery(name = "Coffees.findByTotal", query = "SELECT c FROM Coffees c WHERE c.total = :total"),
    @NamedQuery(name = "Coffees.findByPrice", query = "SELECT c FROM Coffees c WHERE c.price = :price")})
public class Coffees implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "SALES")
    private int sales;
    @Basic(optional = false)
    @Column(name = "COF_NAME")
    private String cofName;
    @Basic(optional = false)
    @Column(name = "TOTAL")
    private int total;
    @Basic(optional = false)
    @Column(name = "PRICE")
    private float price;

    public Coffees() {
    }

    public Coffees(Integer id) {
        this.id = id;
    }

    public Coffees(Integer id, int sales, String cofName, int total, float price) {
        this.id = id;
        this.sales = sales;
        this.cofName = cofName;
        this.total = total;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public String getCofName() {
        return cofName;
    }

    public void setCofName(String cofName) {
        this.cofName = cofName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coffees)) {
            return false;
        }
        Coffees other = (Coffees) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Coffees[ id=" + id + " ]";
    }
    
}
