/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "propertyforrent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Propertyforrent.findAll", query = "SELECT p FROM Propertyforrent p"),
    @NamedQuery(name = "Propertyforrent.findByPropertyNo", query = "SELECT p FROM Propertyforrent p WHERE p.propertyNo = :propertyNo"),
    @NamedQuery(name = "Propertyforrent.findByStreet", query = "SELECT p FROM Propertyforrent p WHERE p.street = :street"),
    @NamedQuery(name = "Propertyforrent.findByCity", query = "SELECT p FROM Propertyforrent p WHERE p.city = :city"),
    @NamedQuery(name = "Propertyforrent.findByPostcode", query = "SELECT p FROM Propertyforrent p WHERE p.postcode = :postcode"),
    @NamedQuery(name = "Propertyforrent.findByType", query = "SELECT p FROM Propertyforrent p WHERE p.type = :type"),
    @NamedQuery(name = "Propertyforrent.findByRooms", query = "SELECT p FROM Propertyforrent p WHERE p.rooms = :rooms"),
    @NamedQuery(name = "Propertyforrent.findByRent", query = "SELECT p FROM Propertyforrent p WHERE p.rent = :rent"),
    @NamedQuery(name = "Propertyforrent.findByPicture", query = "SELECT p FROM Propertyforrent p WHERE p.picture = :picture"),
    @NamedQuery(name = "Propertyforrent.findByFloorPlan", query = "SELECT p FROM Propertyforrent p WHERE p.floorPlan = :floorPlan")})
public class Propertyforrent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "propertyNo")
    private String propertyNo;
    @Basic(optional = false)
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @Column(name = "rooms")
    private int rooms;
    @Basic(optional = false)
    @Column(name = "rent")
    private int rent;
    @Basic(optional = false)
    @Column(name = "picture")
    private String picture;
    @Basic(optional = false)
    @Column(name = "floorPlan")
    private String floorPlan;
    @JoinColumn(name = "branchNo", referencedColumnName = "branchNo")
    @ManyToOne(optional = false)
    private Branch branchNo;
    @JoinColumn(name = "ownerNo", referencedColumnName = "ownerNo")
    @ManyToOne(optional = false)
    private Privateowner ownerNo;
    @JoinColumn(name = "staffNo", referencedColumnName = "staffNo")
    @ManyToOne(optional = false)
    private Staff staffNo;
    @OneToMany(mappedBy = "propertyNo")
    private Collection<Viewing> viewingCollection;

    public Propertyforrent() {
    }

    public Propertyforrent(String propertyNo) {
        this.propertyNo = propertyNo;
    }

    public Propertyforrent(String propertyNo, String street, String city, String postcode, String type, int rooms, int rent, String picture, String floorPlan) {
        this.propertyNo = propertyNo;
        this.street = street;
        this.city = city;
        this.postcode = postcode;
        this.type = type;
        this.rooms = rooms;
        this.rent = rent;
        this.picture = picture;
        this.floorPlan = floorPlan;
    }

    public String getPropertyNo() {
        return propertyNo;
    }

    public void setPropertyNo(String propertyNo) {
        this.propertyNo = propertyNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan(String floorPlan) {
        this.floorPlan = floorPlan;
    }

    public Branch getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(Branch branchNo) {
        this.branchNo = branchNo;
    }

    public Privateowner getOwnerNo() {
        return ownerNo;
    }

    public void setOwnerNo(Privateowner ownerNo) {
        this.ownerNo = ownerNo;
    }

    public Staff getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(Staff staffNo) {
        this.staffNo = staffNo;
    }

    @XmlTransient
    public Collection<Viewing> getViewingCollection() {
        return viewingCollection;
    }

    public void setViewingCollection(Collection<Viewing> viewingCollection) {
        this.viewingCollection = viewingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (propertyNo != null ? propertyNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Propertyforrent)) {
            return false;
        }
        Propertyforrent other = (Propertyforrent) object;
        if ((this.propertyNo == null && other.propertyNo != null) || (this.propertyNo != null && !this.propertyNo.equals(other.propertyNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Propertyforrent[ propertyNo=" + propertyNo + " ]";
    }
    
}
