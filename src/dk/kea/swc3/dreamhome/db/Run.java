/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author alex
 */
public class Run {

    public static void main(String[] args) {
        EntityManager em = Persistence.createEntityManagerFactory("PersistMySQLJavaDreamHomePU").createEntityManager();
        Query query2 = em.createNamedQuery("Propertyforrent.findByPropertyNo").setParameter("propertyNo", "PA14");
        List<Propertyforrent> propertyList = query2.getResultList();

        Propertyforrent p = propertyList.get(0);
        System.out.println("before --> " + p.getCity());

        p.setCity("CPH");

        System.out.println("after --> " + p.getCity());

        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();

        /*
         Query query = em.createNamedQuery("Propertyforrent.findAll");
        
         List<Propertyforrent> pfrList = query.getResultList();
        
        
        
         for (Propertyforrent pfrList1 : pfrList) {
         System.out.println("------------------");
         System.out.println("Property: "+pfrList1.getPropertyNo());
         System.out.println("\n Viewings");
         Collection<Viewing> viewings = pfrList1.getViewingCollection();
            
         for (Viewing viewing : viewings) {
         System.out.println(viewing);
         }
            
         }
         */
    }
}
