/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc3.dreamhome.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alex
 */
@Entity
@Table(name = "privateowner")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Privateowner.findAll", query = "SELECT p FROM Privateowner p"),
    @NamedQuery(name = "Privateowner.findByOwnerNo", query = "SELECT p FROM Privateowner p WHERE p.ownerNo = :ownerNo"),
    @NamedQuery(name = "Privateowner.findByFName", query = "SELECT p FROM Privateowner p WHERE p.fName = :fName"),
    @NamedQuery(name = "Privateowner.findByLName", query = "SELECT p FROM Privateowner p WHERE p.lName = :lName"),
    @NamedQuery(name = "Privateowner.findByAddress", query = "SELECT p FROM Privateowner p WHERE p.address = :address"),
    @NamedQuery(name = "Privateowner.findByTelNo", query = "SELECT p FROM Privateowner p WHERE p.telNo = :telNo"),
    @NamedQuery(name = "Privateowner.findByEMail", query = "SELECT p FROM Privateowner p WHERE p.eMail = :eMail"),
    @NamedQuery(name = "Privateowner.findByPassword", query = "SELECT p FROM Privateowner p WHERE p.password = :password")})
public class Privateowner implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ownerNo")
    private String ownerNo;
    @Basic(optional = false)
    @Column(name = "fName")
    private String fName;
    @Basic(optional = false)
    @Column(name = "lName")
    private String lName;
    @Basic(optional = false)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @Column(name = "telNo")
    private String telNo;
    @Basic(optional = false)
    @Column(name = "eMail")
    private String eMail;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ownerNo")
    private Collection<Propertyforrent> propertyforrentCollection;

    public Privateowner() {
    }

    public Privateowner(String ownerNo) {
        this.ownerNo = ownerNo;
    }

    public Privateowner(String ownerNo, String fName, String lName, String address, String telNo, String eMail, String password) {
        this.ownerNo = ownerNo;
        this.fName = fName;
        this.lName = lName;
        this.address = address;
        this.telNo = telNo;
        this.eMail = eMail;
        this.password = password;
    }

    public String getOwnerNo() {
        return ownerNo;
    }

    public void setOwnerNo(String ownerNo) {
        this.ownerNo = ownerNo;
    }

    public String getFName() {
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getLName() {
        return lName;
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public Collection<Propertyforrent> getPropertyforrentCollection() {
        return propertyforrentCollection;
    }

    public void setPropertyforrentCollection(Collection<Propertyforrent> propertyforrentCollection) {
        this.propertyforrentCollection = propertyforrentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ownerNo != null ? ownerNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privateowner)) {
            return false;
        }
        Privateowner other = (Privateowner) object;
        if ((this.ownerNo == null && other.ownerNo != null) || (this.ownerNo != null && !this.ownerNo.equals(other.ownerNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dk.kea.swc3.dreamhome.db.Privateowner[ ownerNo=" + ownerNo + " ]";
    }
    
}
